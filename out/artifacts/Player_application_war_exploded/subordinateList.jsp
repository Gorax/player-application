<%@ page import="java.util.List" %>
<%@ page import="src.model.Playlist" %>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Player application</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="">Player application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="playlist">Set playlist</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="player">Player</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!-- Page Content -->
<div class="container">
    <%
        List mediaList = (List) request.getAttribute("mediaList");
        Playlist subordinateList = (Playlist) request.getAttribute("subordinate");
    %>

    <div class="row">

        <div class="col-lg-3" style="margin-top: 8%">
            <div class="list-group">
                <a class="list-group-item">Choose your playlist:
                </a>
            </div>

            <div class="list-group">
                <a href="playlist" class="list-group-item">Main list</a>
            </div>
            <div class="list-group">
                <a href="" class="list-group-item active"><%=subordinateList.getName()%>
                </a>
            </div>
        </div>

        <div class="border" style="width: 30%; margin-top: 8%;">
            <div class="card-header">
                In your playList:
                <%--<br><a href="">Clear the playlist</a>--%>
            </div>
            <div class="card-body">
                <%
                    if (subordinateList != null && !subordinateList.getPlaylist().isEmpty()) {
                        for (Object element : subordinateList.getPlaylist()) {
                %>
                <ol>
                    <a>
                        <%=element.toString()%>
                    </a>
                </ol>
                <%
                    }
                } else {
                %>
                <a>You haven't any media file.</a>
                <%
                    }
                %>
            </div>
        </div>

        <div class="border" style="width: 40%; margin-top: 8%; margin-left: 2%">
            <div class="card-header">
                Your all media files:
            </div>
            <div class="card-body">
                <%
                    if (mediaList != null && !mediaList.isEmpty()) {
                        for (Object element : mediaList) {
                %>
                <ol>
                    <form method="post" action="subordinateList">
                        <a><%=element.toString()%>
                        </a>
                        <input name="element" type="hidden" value="<%=element%>">
                        <button style="margin-left: 3%" type="submit" class="btn btn-primary">add</button>
                    </form>
                </ol>
                <%
                    }
                } else {
                %>
                <a>You haven't any media file.</a>
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>

</body>

</html>
