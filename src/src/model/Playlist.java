package src.model;

import java.util.ArrayList;
import java.util.List;

public class Playlist {

    private String name;
    private String typePlayback;
    private List playList;

    public Playlist(String name, String typePlayback) {
        this.name = name;
        this.typePlayback = typePlayback;
        this.playList = new ArrayList();
    }

    public String getName() {
        return name;
    }

    public List getPlaylist() {
        return playList;
    }

    public String getTypePlayback() {
        return typePlayback;
    }

    public void setTypePlayback(String typePlayback) {
        this.typePlayback = typePlayback;
    }

    @Override
    public String toString() {
        return name;
    }
}
