package src.service;

import src.model.Playlist;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Stateless
public class PlayerService {

    @EJB
    private PlaylistService playlistService;

    private List sortedSubordinateList = new ArrayList();

    private ArrayList<Integer> numbers = new ArrayList<>();

    public Playlist sortMainList() {
        Playlist sortedMainList = new Playlist("Main list", playlistService.getMainPlaylist().getTypePlayback());

        if (playlistService.getMainPlaylist().getTypePlayback().equals("Random")) {
            random(playlistService.getMainPlaylist().getPlaylist().size());
            for (int i = 0; i < playlistService.getMainPlaylist().getPlaylist().size(); i++) {
                sortedMainList.getPlaylist().add(playlistService.getMainPlaylist().getPlaylist().get(numbers.get(i)));
            }
        } else {
            sortedMainList.getPlaylist().addAll(sortedSubordinateList);
        }
        return sortedMainList;
    }

    public void sortSubordinateList() {
        sortedSubordinateList.clear();
        for (Object element : playlistService.getMainPlaylist().getPlaylist()) {
            if (element.getClass() == Playlist.class) {
                Playlist playlist = (Playlist) element;
                if (playlist.getTypePlayback().equals("Sequential")) {
                    sortedSubordinateList.add(playlist.getPlaylist());
                } else if (playlist.getTypePlayback().equals("Loop")) {
                    sortedSubordinateList.add(playlist.getPlaylist());
                    sortedSubordinateList.add(playlist.getName() + " are looped");
                    break;
                } else {
                    List randomList = new ArrayList();
                    random(playlist.getPlaylist().size());
                    for (int i = 0; i < playlist.getPlaylist().size(); i++) {
                        randomList.add(playlist.getPlaylist().get(numbers.get(i)));
                    }
                    sortedSubordinateList.add(randomList);
                }
            } else {
                sortedSubordinateList.add(element);
            }
        }
    }

    private void random(int range) {
        numbers.clear();

        Random randomGenerator = new Random();

        while (numbers.size() < range) {

            int random = randomGenerator.nextInt(range);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }
    }
}