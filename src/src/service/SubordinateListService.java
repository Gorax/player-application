package src.service;

import src.model.Playlist;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class SubordinateListService {

    @EJB
    private PlaylistService playlistService;

    public void addMediaToSubordinateList (Object media, int index){
       Playlist playlist = (Playlist) playlistService.getMainPlaylist().getPlaylist().get(index);
       playlist.getPlaylist().add(media);
    }
}

