package src.service;

import src.dataBase.DataBase;
import src.model.AudioFile;
import src.model.VideoFile;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class AddFileService {

    @EJB
    private DataBase dataBase;

    public void addMp3 (String name) {
        AudioFile audioFile = new AudioFile(name);
        dataBase.getListOfMediaFile().add(audioFile);
    }

    public  void addMp4(String name){
        VideoFile videoFile = new VideoFile(name);
        dataBase.getListOfMediaFile().add(videoFile);
    }
}
