package src.service;

import src.dataBase.DataBase;
import src.model.Playlist;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class PlaylistService {

    @EJB
    private DataBase dataBase;

    private Playlist mainPlaylist = new Playlist("Main playlist", "Sequential");

    public Playlist getMainPlaylist() {
        return mainPlaylist;
    }

    public List getMediaFile (){
        return dataBase.getListOfMediaFile();
    }

    public void addMediaToMainList (Object media){
        mainPlaylist.getPlaylist().add(media);
    }

    public void addPlaylist (String name, String type){
        mainPlaylist.getPlaylist().add(new Playlist(name, type));
    }

    public void clearThePlaylist (){
        mainPlaylist.getPlaylist().clear();
    }

    public void changeMainListType (String type){
        mainPlaylist.setTypePlayback(type);
    }

}
