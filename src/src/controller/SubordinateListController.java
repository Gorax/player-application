package src.controller;

import src.service.PlaylistService;
import src.service.SubordinateListService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/subordinateList")
public class SubordinateListController extends HttpServlet {

    @EJB
    private PlaylistService playlistService;

    @EJB
    private SubordinateListService subordinateListService;

    private int index = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        for (int i = 0; i < playlistService.getMainPlaylist().getPlaylist().size(); i++) {
            if (req.getParameter("" + i) != null) {
                index = i;
                req.setAttribute("subordinate", playlistService.getMainPlaylist().getPlaylist().get(i));
                req.setAttribute("mediaList", playlistService.getMediaFile());
                req.getRequestDispatcher("subordinateList.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Object object = req.getParameter("element");
        if (object != null)
            subordinateListService.addMediaToSubordinateList(object, index);
        resp.sendRedirect(resp.encodeRedirectURL("subordinateList?" + index));
    }
}

