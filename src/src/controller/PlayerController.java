package src.controller;

import src.service.PlayerService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/player")
public class PlayerController extends HttpServlet {

    @EJB
    private PlayerService playerService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        playerService.sortSubordinateList();
        req.setAttribute("toPlay", playerService.sortMainList());
        req.getRequestDispatcher("player.jsp").forward(req, resp);
    }
}
