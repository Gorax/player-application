package src.controller;

import src.service.PlaylistService;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addPlaylist")
public class AddPlaylistController extends HttpServlet {

    @EJB
    private PlaylistService playlistService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        playlistService.addPlaylist(req.getParameter("name"), req.getParameter("type"));
        resp.sendRedirect(resp.encodeRedirectURL("playlist"));
    }
}
