package src.controller;

import src.service.PlaylistService;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/clear")
public class ClearPlaylist extends HttpServlet {

    @EJB
    private PlaylistService playlistService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        playlistService.clearThePlaylist();
        resp.sendRedirect(resp.encodeRedirectURL("playlist"));
    }
}
