package src.controller;

import src.service.PlaylistService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/playlist")
public class PlaylistController extends HttpServlet {

    @EJB
    private PlaylistService playlistService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("mediaList", playlistService.getMediaFile());
        req.setAttribute("mainPlaylist", playlistService.getMainPlaylist());
        req.getRequestDispatcher("playlist.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Object object = req.getParameter("element");
        if (object != null){
            playlistService.addMediaToMainList(object);
            resp.sendRedirect(resp.encodeRedirectURL("playlist"));
        }else{
            resp.sendRedirect(resp.encodeRedirectURL("playlist?fakap"));
        }
    }
}
