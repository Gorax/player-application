package src.controller;

import src.service.PlaylistService;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/changeMainListType")
public class ChangeMainListTypeController extends HttpServlet {

    @EJB
    private PlaylistService playlistService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String random = req.getParameter("random");
        String loop = req.getParameter("loop");
        String sequential = req.getParameter("sequential");
        if (random != null) {
            playlistService.changeMainListType("Random");
        } else if (loop != null) {
            playlistService.changeMainListType("Loop");
        } else if (sequential != null) {
            playlistService.changeMainListType("Sequential");
        }
        resp.sendRedirect(resp.encodeRedirectURL("playlist"));
    }
}
