package src.controller;

import src.service.AddFileService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Paths;

@WebServlet("/home")
@MultipartConfig
public class HomeController extends HttpServlet {

    @EJB
    private AddFileService addFileService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(resp.encodeRedirectURL("home.jsp"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("file");
        String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
        if (validate(fileName)) {
            if (fileName.endsWith(".mp3")) {
                addFileService.addMp3(fileName);
            } else if (fileName.endsWith(".mp4")) {
                addFileService.addMp4(fileName);
            }
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL("home.jsp?invalidData"));
        }
    }

    private boolean validate(String fileName) {
        if (fileName != null && !fileName.isEmpty() && fileName.endsWith(".mp3") || fileName.endsWith(".mp4"))
            return true;
        return false;
    }
}
