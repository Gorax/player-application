<%@ page import="java.util.List" %>
<%@ page import="src.model.Playlist" %>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Player application</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-item.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="">Player application</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="home">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="playlist">Set playlist</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="player">Player</a>
                </li>
            </ul>
        </div>
    </div>
</nav>


<!-- Page Content -->
<div class="container">
    <%
        Playlist mainList = (Playlist) request.getAttribute("toPlay");
    %>

    <div class="row">

        <div class="col-lg-3" style="margin-top: 8%">
            <div class="list-group">
                <%--<a href="player?play" class="list-group-item">Play</a>--%>
            </div>
        </div>

        <div class="col-lg-9">
            <h2 style="text-align: center; margin-top: 3%">Player application</h2>
            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Now played:
                    <%
                        if ("Loop".equals(mainList.getTypePlayback())) {
                    %>
                    <a>Playlist looped</a>
                    <%
                        }
                    %>
                </div>
                <ol>
                    <%
                        if (mainList != null && !mainList.getPlaylist().isEmpty()) {
                            for (Object element : mainList.getPlaylist()) {

                    %>
                    <div class="card-body">
                        <li><%=element.toString()%>
                        </li>
                    </div>
                    <%
                        }
                    } else {
                    %>
                    <div class="card-body">
                        <a>There is nothing to play.</a>
                    </div>
                    <%
                        }
                    %>
                </ol>
            </div>
        </div>
    </div>

</div>

</body>

</html>
